    #include <linux/kernel.h>
    #include <linux/module.h>
    #include <linux/proc_fs.h>
    #include <linux/uaccess.h>
    #include <linux/version.h>
    #include <linux/ktime.h>

    #define PROCFS_NAME "tsu"

    static struct proc_dir_entry *our_proc_file = NULL;

    static ssize_t tsu_procfile_read(struct file *file, char __user *buffer, size_t count, loff_t *offset) {
        static int tsu_completed = 0;
        char tsu_message[50];
        s64  uptime;

        if (*offset >= snprintf(tsu_message, sizeof(tsu_message), "Tomsk\n")) {
            return 0;
        }

        uptime = ktime_divns(ktime_get_coarse_boottime(), 60 * NSEC_PER_SEC);
        snprintf(tsu_message, sizeof(tsu_message), "Tomsk\nMinutes since boot: %lld\n", uptime);

        if (count > strlen(tsu_message) - *offset) {
            count = strlen(tsu_message) - *offset;
        }

        tsu_completed = 1;

        if (copy_to_user(buffer, tsu_message + *offset, count)) {
            pr_err("Error copying to user\n");
            return -EFAULT;
        }

        pr_info("tsu_procfile read %s\n", file->f_path.dentry->d_name.name);
        *offset += count;

        return count;
    }

    #if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 6, 0)
    static const struct proc_ops proc_file_fops = {
        .proc_read = tsu_procfile_read,
    };
    #else
    static const struct file_operations proc_file_fops = {
        .read = tsu_procfile_read,
    };
    #endif

    static int __init tsu_module_init(void) {
        our_proc_file = proc_create(PROCFS_NAME, 0644, NULL, &proc_file_fops);
        if (!our_proc_file) {
            pr_err("Failed to create proc file\n");
            return -ENOMEM;
        }

        pr_info("Welcome to the Tomsk State University\n");
        return 0;
    }

    static void __exit tsu_module_exit(void) {
        proc_remove(our_proc_file);
        pr_info("Tomsk State University forever!\n");
    }

    module_init(tsu_module_init);
    module_exit(tsu_module_exit);

    MODULE_LICENSE("GPL");
